# CESI Projet coeur

réalisé par Fadi, Filémon, Jean-Phillipe et Adrien

# La partie code en C

Nous allons décomposer le code en C pour mieux pouvoir l'expliquer.  
On va commencer par le main.c   

La premiere chose à faire est d'ajouter les librairies necessaire ainsi celle que l'on va créer. 
``` bash
#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "menu.c"
#include "actions.h"
#include "actions.c"
```
Ensuite on déclare nos fonctions, que l'on detaillera peu apres, de la manière suivantes:
``` bash
int loadfile();                          
void croissant();
void decroissant();
```
Puis on déclare des tableaux
``` bash
int T1[100];                           
int T2[100];
```
Dans le int main il va être necessaire de déclarer certaines variable ainsi que nos différentes fonctions:
Egalement il va être necessaire d'utiliser la fonciton menu, pour l'affichage, et loadfile pour utiliser nos tableau, fonctions que l'on détaillera après.  

``` bash
int main()
{                       
int indice;
int a,choitri;
indice=loadfile();
printf("Index %d\n",indice);
a=menu();     
```
Par la suite, toujours dans le main, on utilise des structures de controles switch, case (équivalent au if, else)
afin de laisser l'utilisateur choisir ce qu'il veut faire.  
``` bash
switch (a){
case 1:
    dispfile(T1,T2,indice);         //appelle fonction dispfile de paramètre T1, T2, indice
    break;
case 2:
   choitri =tri();                   //appelle de la fonction qui créé le sous menu
    switch (choitri){               //Création d'un sous menu
case 1:
croissant(indice);                      //appelle e la fonction croissant de paramètre indice
    break;
case 2:
decroissant(indice);                    //appelle de la fonction decroissant de paramètre indice
    break;
default :
    kickback();                         //appelle de la fonction kickback
    break;
}
    break;
case 3:
moyenne(T1,T2);                         //appelle de la fonction moyenne de paramètre T1, T2
break;
case 4:
    ind(indice);
    break;
case 5:
    amplitude(T1,T2,indice);                    //appelle de la fonction amplitude de paramètre T1, T2, indice
case 0:
    kickback();                                 //kickback permet de sortir du menu des choix
    break;
}
    return 0;
}
```
Après avoir quitter le int main  on va s'occuper de créer la fonction loadfile, cette fontion permet le chargement des tableaux et de leur contenu:

``` bash
int loadfile(){                                     
int i=0,val1,val2;
FILE* fp;
fp=fopen ("Bpm.csv","r");
while (!feof(fp)){
    fscanf(fp,"%d ; %d",&val1,&val2);
    T1[i]=val1;
    T2[i]=val2;
    i++;
   }
int index=i;
i=0;
return index;
}
```
La fonction croissant de paramètre indice est la fonction qui permet le tri (tri à bulle) des valeurs de pouls de manière croissante:
``` bash
void croissant(indice){                         
int i=1,j,a=0,n=0,c;                           
c=indice;                                      
for (i=0; i<c; i++){
    for(j=i; j<c; j++){
        if(T1[j]<T1[i]) {
a = T1[i];
T1[i] = T1[j];
T1[j] =a;

a = T2[i];
T2[i] = T2[j];
T2[j] =a;
}}}
dispfile(T1,T2,c);               (dispfile est une fonction créer afin de pouvoir afficher le fichier dans ce cas de paramètre T1, T2, c) 
}
```
Enfin la fonction décroissant est l'exacte opposé de la fonction croissant, elle permet de trier (tri à bulle) de manière décroissante les valeurs de pouls:
``` bash
void decroissant(indice){                      
int i=1,j,a=0,n=0,c;                           
c=indice;                                      
for (i=0; i<c; i++){
    for(j=i; j<c; j++){
        if(T1[j]>T1[i]) {
a = T1[i];
T1[i] = T1[j];
T1[j] =a;

a = T2[i];
T2[i] = T2[j];
T2[j] =a;
            }
        }

     }


dispfile(T1,T2,c);                              
}
```

Dans le menu.c on créé la fonction menu:
``` bash
int menu(){
int a;
printf("Menu de traitement de donnee du Cardiofrequencemetre");
printf("\n1 - Afficher le contenu du fichier\n2 - Afficher les données ordonnées \n3 - Rechercher sur une plage de temps\n4 - Afficher le Nombre de données\n5 - Rechercher les valeurs maxi et mini et calculer l'amplitude\n0 - Quitter L'application\n");
scanf("%d",&a);
return a;
}
```
Dans le menu.h on détaille de quoi va être composé notre librairie menu.h, c'est à dire de la fonction menu 
``` bash
#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED
int menu();
#endif
```



Dans le action.c, on iclude dans un premier temps les bibliothèque necessaire:
``` bash
#include <stdio.h>
#include <math.h>
#include "actions.h"
```
la fonction tri permet de demander à l'utilisateur quel tri (croissant ou decroissant) doit être realiser
``` bash
int tri(){                                  
    int a;
printf("Voulez vous un fichier trié par ordre croissant ou décroissant?\n1 - Croissant\n2 - Décroissant\n");
scanf("%d",&a);
return a;
}
```
La fonction kickback permet de quitter le programme :
``` bash
void kickback(){                            //Fonction qui permet de quitter le menu, la console
printf("Vous venez de quitter le programme\n");
exit(1);
}
```
La fonction moyenne permet de calculer la moyenne des pouls recuperes dans un intervalle donne de temps
``` bash
void moyenne(int T1[],int T2[]){            
    int a,b;
    int i=0,index1,index2,indextot,totl=0;
    int moyn;
printf("Veuillez entrer la plage de temps\n");
a=my_put_char();
b = my_put_char();
while (T2[i]!=a)
{
    i++;
}
index1=i;
i=0;
while (T2[i]!=b)
{
    i++;
}
index2=i;
indextot=index2-index1+1;
i=index1;
while (i<=index2){
    totl=totl+T1[i];
    i++;
}
moyn=totl/indextot;
printf("La moyenne est %d",moyn);
}
```
Cette fonction permet de recuperer des valeurs (remplace le scanf)
``` bash
int my_put_char(){                                  
int a;
scanf("%d",&a);
return a;
}
```
Cette fonction permet d'afficher le nombres de lignes contenu dans un fichier
``` bash
void ind(int indice){                                               //Fonction qui nous afficher le nombre de ligne dans le fichier traité
    int c=indice;
printf("Il y a %d lignes dans le fichier traite",c);
}
```
Cette fonction permet d'afficher le plus haute ainsi que le plus basse valeur recuperee dans le fichier, aussi cette fonction peut faire la difference entre les deux valeurs
```bash
void amplitude(int T1[],int T2[],int indice){                       
    int i=0,ind,a=0,c,b;                            
    ind=indice;
while (i<=ind)                                                       //recherche séquentielle pour trouver la valeur (pouls) la plus haute
{
    if (T1[i]>a){
        a=T1[i];
        c=T2[i];
    }
    i++;
}
i=0;
printf("Pouls Maxi : %d Timestamp : %d\n",a,c);
b=a;
while (i<=ind)                                                      //recherche séquentielle pour trouver la valeur (pouls) la plus basse
{
    if (T1[i]<a && T1[i]!=0){
        a=T1[i];
        c=T2[i];
    }
    i++;
}
printf("Pouls Mini : %d Timestamp : %d\n",a,c);
c=b-a;                                                              //calcul et affichage de l'amplitude

printf("Amplitude : %d\n",c);
}
```




Cette fonction permet d'afficher le contenu d'un fichier
``` bash
void dispfile(int T1[],int T2[],int indice){                      
int i=0;
while(i<indice){
    printf("Pouls : %d Timestamp : %d\n",T1[i],T2[i]);
    i++;
}
}
```

Pareil que pour le menu.h, dans le action.h, dans ce cas on appelle nos différentes fonctions
``` bash
#ifndef ACTIONS_H_INCLUDED
#define ACTIONS_H_INCLUDED
int my_swap(int a, int b);
void kickback();
int tri();
void moyenne(int T1[],int T2[]);
void ind(int indice);
void amplitude(int T1[],int T2[],int indice);

void dispfile(int T1[],int T2[],int indice);
int my_put_char();
#endif // ACTIONS_H_INCLUDED
```
# Le code Arduino

Voici le code Arduino commenté, ce code permet l'utilisation des différents modes demandés ainsi qu'à detecter le poul 
``` bash
#define ANALOG_IN 0         //Definit les entrées et la vitesse de comminication avec le port serie
#include "param.h"          //inclut les données de param.h
#define BAUDRATE 9600

int beat;                 //definit les variables globales que l'on va utiliser
int buzzer = 11;
unsigned long temps;

void setup() {
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(buzzer, OUTPUT);
  tone(buzzer, 500);
  Serial.begin(BAUDRATE);               //définit les pins et un buzzer en sortie

}



void compteur() {                            //fonction toujours utilisée
  temps = millis() / 1000;                   //temps correspond a compte automatique converti en secondes
  int val1 = analogRead(ANALOG_IN);          //lit la valeur analogique fournie par le circuit du module 1
  int bpm;
  if (val1 >= 100) {                        //SI un battement est détecté
    tone(buzzer, 500);
    digitalWrite(10, 255);
    delay(100);
    noTone(buzzer);
    digitalWrite(10, 0);
    beat++;                                 //incrémentation de la valeur battement
    Serial.print((beat * 60) / temps);      // calcul du pouls
    Serial.print(";");
    Serial.print(temps);
    //affichage ordonné de la valeur du pouls à chaque mesure réalisée
  }

  delay(300);
}



void toutourien() {
  int val1 = analogRead(ANALOG_IN);//lit la valeur analogique fournie par le circuit du module 1
  if (val1 >= 100) {                     //Si un battement est détecté

    digitalWrite(1, HIGH);
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
    digitalWrite(6, HIGH);
    digitalWrite(7, HIGH);
    digitalWrite(8, HIGH);          //Allume toutes les DEL
  } else {                          //Sinon
    digitalWrite(1, LOW);           // Eteint toutes les DEL
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
    digitalWrite(6, LOW);
    digitalWrite(7, LOW);
    digitalWrite(8, LOW);
  }
}
void unsurdeux() {
  int val1 = analogRead(ANALOG_IN);      //lit la valeur analogique fournie par le circuit du module 1
  if (val1 >= 100) {                     //Si un battement est détecté

    digitalWrite(1, 255);
    digitalWrite(2, 0);
    digitalWrite(3, 255);
    digitalWrite(4, 0);
    digitalWrite(5, 255);                //Allume une DEL sur deux
    digitalWrite(6, 0);
    digitalWrite(7, 255);
    digitalWrite(8, 0);
  } else {
    digitalWrite(1, 0);
    digitalWrite(2, 0);                   // Puis les éteint quand le battement n'est plus détecté
    digitalWrite(3, 0);
    digitalWrite(4, 0);
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(7, 0);
    digitalWrite(8, 0);
  }
}
void unsurtrois() {
  int val1 = analogRead(ANALOG_IN);             //lit la valeur analogique fournie par le circuit du module 1
  if (val1 >= 100) {                            //Si un battement est détecté

    digitalWrite(1, 255);
    digitalWrite(2, 0);
    digitalWrite(3, 0);
    digitalWrite(4, 255);                        //Allume une DEL sur trois
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(7, 255);
    digitalWrite(8, 0);
  } else {
    digitalWrite(1, 0);
    digitalWrite(2, 0);
    digitalWrite(3, 0);
    digitalWrite(4, 0);                           // Puis les éteint quand le battement n'est plus détecté
    digitalWrite(5, 0);
    digitalWrite(6, 0);
    digitalWrite(7, 0);
    digitalWrite(8, 0);
  }
}

void chenille() {
  int tab = 1;
  
  int maxLed = 8;                                 //Définit les variables nécessaire pour réaliser la "chenille"
  while (1) {
    int val1 = analogRead(ANALOG_IN);             //lit la valeur analogique fournie par le circuit du module 1
    if (val1 >= 100)                              //Si un battement est détecté
    {
      digitalWrite(tab - 1, LOW);                 //éteindre la DEL précédente
      digitalWrite(tab, HIGH);                    //Allumer la suivante

      delay(10);

      tab++;                                      //Incrémenter le numéro de DEL allumée
      delay(200);
    }
    if (tab > maxLed)                             //Si le numéro de la led dépasse le nombre de DEL totales
    {
      digitalWrite(maxLed, 0);                    //Eteindre La derniere DEL
      tab = 1;                                    //Replacer le numéro de DEL à 1
    }
    delay(200);
    digitalWrite(tab, 0);
  }
}

void loop() {
#ifdef CHENILLE                                    //Lance le compteur et le mode choisi si la fonction est définie dans param.h
  chenille();
  compteur();

#endif
#ifdef TOUTOURIEN
  toutourien();
  compteur();
#endif
#ifdef UNSURDEUX
  unsurdeux();
  compteur();
#endif
#ifdef UNSURTROIS
  unsurtrois();
  compteur();
#endif

}
```